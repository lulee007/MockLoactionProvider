package com.szmap.model;

import android.content.Context;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;

import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.MapOnTouchListener;
import com.esri.android.map.MapView;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.Polyline;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.LineSymbol;
import com.esri.core.symbol.MarkerSymbol;
import com.esri.core.symbol.SimpleLineSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;

/**
 * 
 * 
 * 画图实现类，支持画点、矩形、线、多边形、圆、手画线、手画多边形，可设置各种图形的symbol。
 */
public class DrawTool {

	private MapView mapView;
	private GraphicsLayer tempLayer;
	private MarkerSymbol markerSymbol;
	private LineSymbol lineSymbol;
	private int drawType;
	private boolean active;
	private Point point;
	private Polyline polyline;
	private DrawTouchListener drawListener;
	private MapOnTouchListener defaultListener;
	private Graphic drawPolylineGraphic;
	private Graphic drawPointGraphic;
	private CallBackDrawGraphic callback;
	public  boolean finished = false;
	public static final int POINT = 1;
	public static final int POLYLINE = 3;
	public static final int FREEHAND_POLYLINE = 8;
	public static final int DRAW_BEGIN = 0x100;
	public static final int DRAW_CANCEL = 0x101;
	public static final int DRAW_COMPLETE = 0x102;
	public static final int DRAW_MOVE =0x103;
	public static final int DRAW_SAVE = 0x104;
	
	public boolean disablePan=true;
	private int POLYLINE_ID = -0x999;
	private int POINT_ID = -0x999;
	public boolean draw_begin=false;
	public boolean draw_cancel=false;

	public DrawTool(MapView mapView, CallBackDrawGraphic callbackdrawgraphic) {
		this.mapView = mapView;
		this.tempLayer = new GraphicsLayer();
		this.mapView.addLayer(this.tempLayer);
		this.callback = callbackdrawgraphic;
		drawListener = new DrawTouchListener(this.mapView.getContext(),
				this.mapView);
		defaultListener = new MapOnTouchListener(this.mapView.getContext(),
				this.mapView);
		this.markerSymbol = new SimpleMarkerSymbol(Color.BLACK, 16,
				SimpleMarkerSymbol.STYLE.CIRCLE);
		this.lineSymbol = new SimpleLineSymbol(Color.BLACK, 2);
	}

	public void activate(int drawType) {
		if (this.mapView == null)
			return;
		this.mapView.setOnTouchListener(drawListener);
		this.drawType = drawType;
		this.active = true;
		this.finished=false;
		this.disablePan = true;
		this.draw_begin = true;
		drawPolylineGraphic = null;
		drawPointGraphic = null;
		switch (this.drawType) {
		case DrawTool.POINT:
			this.point = new Point();
			drawPointGraphic = new Graphic(this.point, this.markerSymbol);
			POINT_ID = this.tempLayer.addGraphic(drawPointGraphic);
			break;
		case DrawTool.POLYLINE:
		case DrawTool.FREEHAND_POLYLINE:
			this.polyline = new Polyline();
			drawPolylineGraphic = new Graphic(this.polyline, this.lineSymbol);
			POLYLINE_ID = this.tempLayer.addGraphic(drawPolylineGraphic);
			break;
		}
	}

	public void deactivate() {
		this.mapView.setOnTouchListener(defaultListener);
		this.active = false;
		this.drawType = -1;
		this.point = null;
		this.polyline = null;
		this.drawPolylineGraphic = null;
		this.drawPointGraphic = null;
		this.finished=true;
		this.disablePan=false;
		this.draw_cancel=true;
		this.draw_begin = false;
		this.tempLayer.removeAll();
		// this.mapView.removeLayer(tempLayer);
	}

	public MarkerSymbol getMarkerSymbol() {
		return markerSymbol;
	}

	public void setMarkerSymbol(MarkerSymbol markerSymbol) {
		this.markerSymbol = markerSymbol;
	}

	public LineSymbol getLineSymbol() {
		return lineSymbol;
	}

	public void setLineSymbol(LineSymbol lineSymbol) {
		this.lineSymbol = lineSymbol;
	}

	public void sendDrawEndEvent(boolean isfinished) {
		if (finished || isfinished) {
			switch (this.drawType) {
			case DrawTool.POINT:
				this.callback.callback(DrawTool.this.drawPointGraphic);
				break;
			case DrawTool.POLYLINE:
			case DrawTool.FREEHAND_POLYLINE:
				this.callback.callback(DrawTool.this.drawPolylineGraphic);
				break;
			}
//			int type = this.drawType;
			this.deactivate();
//			this.activate(type);

		}
	}

	class DrawTouchListener extends MapOnTouchListener {

		private Point startPoint;

		public DrawTouchListener(Context context, MapView view) {
			super(context, view);
		}

		public boolean onTouch(View view, MotionEvent event) {
			if (active && (drawType == POINT || drawType == FREEHAND_POLYLINE)
					&& event.getAction() == MotionEvent.ACTION_DOWN
					&& disablePan) {
				Point point = mapView.toMapPoint(event.getX(), event.getY());
				switch (drawType) {
				case DrawTool.POINT:
					DrawTool.this.point.setXY(point.getX(), point.getY());
					sendDrawEndEvent(true);
					break;
				case DrawTool.POLYLINE:
				case DrawTool.FREEHAND_POLYLINE:
					if(draw_cancel){
						startPoint = null;
						draw_cancel=false;
					}
					if (startPoint == null) {
						startPoint = new Point(point.getX(), point.getY());
						polyline.startPath(point);
					} else
						polyline.lineTo(point);
					tempLayer.updateGraphic(POLYLINE_ID, polyline);
					break;
				}
				mapView.postInvalidate();
			}
			return super.onTouch(view, event);
		}

		public boolean onDragPointerMove(MotionEvent from, MotionEvent to) {
			if (active && (drawType == FREEHAND_POLYLINE
					&& disablePan)) {
				Point point = mapView.toMapPoint(to.getX(), to.getY());
				switch (drawType) {
				case DrawTool.POLYLINE:
				case DrawTool.FREEHAND_POLYLINE:
					polyline.lineTo(point);
					tempLayer.updateGraphic(POLYLINE_ID, polyline);
					break;
				}
				mapView.postInvalidate();
				return true;
			}
			return super.onDragPointerMove(from, to);
		}

		public boolean onDragPointerUp(MotionEvent from, MotionEvent to) {
			if (active && (drawType == FREEHAND_POLYLINE)
					&& disablePan) {
				Point point = mapView.toMapPoint(to.getX(), to.getY());
				switch (drawType) {
				case DrawTool.POLYLINE:
				case DrawTool.FREEHAND_POLYLINE:
					polyline.lineTo(point);
					tempLayer.updateGraphic(POLYLINE_ID, polyline);
					break;
				}
				if (finished) {
					sendDrawEndEvent(true);
					this.startPoint = null;
				}
				return true;
			}
			return super.onDragPointerUp(from, to);
		}

		public boolean onSingleTap(MotionEvent event) {
			Point point = mapView.toMapPoint(event.getX(), event.getY());
			if (active && (drawType == POLYLINE)
					&& disablePan) {
				switch (drawType) {
				case DrawTool.POLYLINE:
					if (startPoint == null) {
						this.startPoint = point;
						polyline.startPath(point);
					} else
						polyline.lineTo(point);
					break;
				}
				mapView.postInvalidate();
				return true;
			}
			return false;
		}

		public boolean onDoubleTap(MotionEvent event) {
			Point point = mapView.toMapPoint(event.getX(), event.getY());
			if (active && (drawType == POLYLINE)
					&& disablePan) {
				switch (drawType) {
				case DrawTool.POLYLINE:
					polyline.lineTo(point);
					break;
				}
				sendDrawEndEvent(true);
				this.startPoint = null;
			}
			return true;
		}
	}
}