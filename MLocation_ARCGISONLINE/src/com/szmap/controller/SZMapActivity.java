package com.szmap.controller;

public abstract interface SZMapActivity {
	public void initViews();

	public void initListeners();

	public void initParams();

	public void refresh(Object... param);
}
