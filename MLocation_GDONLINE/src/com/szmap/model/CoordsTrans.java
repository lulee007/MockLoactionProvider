package com.szmap.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;

import com.mapabc.mapapi.core.GeoPoint;

/**偏移文本库太大，无法使用，已经弃用
 * @author lxh
 *
 */
public class CoordsTrans {
	double[] TableY = new double[660 * 450];
	boolean InitTable = false;
	private double[] TableX = new double[660 * 450];

	public CoordsTrans(Context context) {

		LoadText(context);
	}

	private int GetID(int I, int J) {
		return I + 660 * J;
	}

	private void LoadText(Context _context) {
		InputStream fis = null;
		try {
			fis = _context.getAssets().open("Mars2Wgs.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (fis == null)
			return;
		BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
		String line = null;
		StringBuffer sb = new StringBuffer();
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Pattern patte = Pattern.compile("(\\d+)");
		Matcher matcher = patte.matcher(sb);
		int i = 0;
		while (matcher.find()) {
			MatchResult m = matcher.toMatchResult();
			if (i % 2 == 0) {
				TableX[i / 2] = Double.valueOf(m.group(0)) / 100000.0;
			} else {
				TableY[(i - 1) / 2] = Double.valueOf(m.group(0)) / 100000.0;
			}
			i++;
		}
		InitTable = true;
	}

	// / <summary>
	// / x是117左右，y是31左右
	// / </summary>
	// / <param name="xMars"></param>
	// / <param name="yMars"></param>
	// / <param name="xWgs"></param>
	// / <param name="yWgs"></param>
	private GeoPoint Parse(double xMars, double yMars) {
		int i, j, k;
		double x1, y1, x2, y2, x3, y3, x4, y4, xtry, ytry, dx, dy;
		double t, u;

		if (!InitTable)
			return null;

		xtry = xMars;
		ytry = yMars;

		for (k = 0; k < 10; ++k) {
			// 只对中国国境内数据转换
			if (xtry < 72 || xtry > 137.9 || ytry < 10 || ytry > 54.9) {
				return null;
			}

			i = (int) ((xtry - 72.0) * 10.0);
			j = (int) ((ytry - 10.0) * 10.0);

			x1 = TableX[GetID(i, j)];
			y1 = TableY[GetID(i, j)];
			x2 = TableX[GetID(i + 1, j)];
			y2 = TableY[GetID(i + 1, j)];
			x3 = TableX[GetID(i + 1, j + 1)];
			y3 = TableY[GetID(i + 1, j + 1)];
			x4 = TableX[GetID(i, j + 1)];
			y4 = TableY[GetID(i, j + 1)];

			t = (xtry - 72.0 - 0.1 * i) * 10.0;
			u = (ytry - 10.0 - 0.1 * j) * 10.0;

			dx = (1.0 - t) * (1.0 - u) * x1 + t * (1.0 - u) * x2 + t * u * x3
					+ (1.0 - t) * u * x4 - xtry;
			dy = (1.0 - t) * (1.0 - u) * y1 + t * (1.0 - u) * y2 + t * u * y3
					+ (1.0 - t) * u * y4 - ytry;

			xtry = (xtry + xMars - dx) / 2.0;
			ytry = (ytry + yMars - dy) / 2.0;
		}
		return new GeoPoint((int) (ytry * 1E6), (int) (xtry * 1E6));
	}

	public GeoPoint TransCoords(GeoPoint geoPoint) {
		double x = geoPoint.getLongitudeE6() / (1E6);
		double y = geoPoint.getLatitudeE6() / (1E6);
		return Parse(x, y);
	}
}
