package com.szmap.model;

import android.os.Bundle;

public interface CallBackBundle {
	    abstract void callback(Bundle bundle);  
}
