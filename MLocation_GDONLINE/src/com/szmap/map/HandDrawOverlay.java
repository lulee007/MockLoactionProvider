package com.szmap.map;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.util.Log;
import android.view.MotionEvent;

import com.mapabc.mapapi.core.GeoPoint;
import com.mapabc.mapapi.map.MapView;
import com.mapabc.mapapi.map.Overlay;

/**
 * 自由绘制线状要素到overlay上
 * 
 * @author lxh
 * 
 */
public class HandDrawOverlay extends Overlay {

	private static final String TAG_CLASS = "HandDrawOverlay";
	private boolean editMode = false;
	private boolean isDrawing = false;
	private Paint paint = new Paint();
	private Point screenPt1 = new Point();
	private Point screenPt2 = new Point();
	private ArrayList<GeoPoint> points = null;
	private boolean disablePan = true;

	public HandDrawOverlay() {
		paint.setAntiAlias(true);
		paint.setStrokeWidth(4.0f);
		paint.setStyle(Style.STROKE);
		paint.setColor(Color.BLUE);
	}

	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		if (points != null && points.size() > 1) {
			mapView.getProjection().toPixels(points.get(0), screenPt1);
			for (int i = 1; i < points.size(); i++) {
				mapView.getProjection().toPixels(points.get(i), screenPt2);
				canvas.drawLine(screenPt1.x, screenPt1.y, screenPt2.x,
						screenPt2.y, paint);
				screenPt1.set(screenPt2.x, screenPt2.y);
			}
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent e, MapView mapView) {
		if (editMode && disablePan) {
			int x = (int) e.getX();
			int y = (int) e.getY();
			GeoPoint geoP = mapView.getProjection().fromPixels(x, y);
			Log.v(TAG_CLASS, geoP.getLatitudeE6() + "," + geoP.getLongitudeE6());
			switch (e.getAction()) {
			case MotionEvent.ACTION_DOWN:
				if (isDrawing) {
					if (points == null) {
						points = new ArrayList<GeoPoint>();
					}
					points.add(geoP);
				}
				break;
			case MotionEvent.ACTION_MOVE:
				if (isDrawing)
					points.add(geoP);
				break;
			case MotionEvent.ACTION_UP:
				if (isDrawing)
					points.add(geoP);
				break;
			}
			mapView.invalidate();
			return true;
		}
		return false;
	}

	/**
	 * @param panAble true不可移动，可以绘制，false可以移动，不可绘制
	 */
	public void setPanControl(boolean panAble){
		disablePan=panAble;
	}
	
	/**
	 * @return 移动锁定的状态
	 */
	public boolean isPanDisable(){
		return disablePan;
	}
	
	/**
	 * 获取自由绘制的状态
	 * 
	 * @return the editMode true可以绘制，false 不可以绘制
	 */
	public boolean isEditMode() {
		return editMode;
	}

	/**
	 * 设置是否可以自由绘制
	 * 
	 * @param editMode
	 *            the editMode to set
	 */
	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
		// 设置绘制模式的资源初始状态
		if (!this.editMode) {
			isDrawing = false;
		} else {
			isDrawing = true;
			if (points != null)
				points.clear();
			points = null;
		}
	}

	/**
	 * 获取已经绘制的路线点集合
	 * 
	 * @return 返回已经选择的路线点集合
	 */
	public ArrayList<GeoPoint> getRoute(Context _context) {
		return points;
	}

}