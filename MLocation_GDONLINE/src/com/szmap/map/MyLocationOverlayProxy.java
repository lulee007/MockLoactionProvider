package com.szmap.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Paint.Style;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;

import com.mapabc.mapapi.core.GeoPoint;
import com.mapabc.mapapi.map.MapView;
import com.mapabc.mapapi.map.Overlay;
import com.mapabc.mapapi.map.Projection;
import com.mapabc.mapapi.map.MyLocationOverlay;
import com.szmap.luleemlocation.R;

/**
 * 
 * 自定义MyLocationOverlay。更换MyLocation图标。
 */
public class MyLocationOverlayProxy extends Overlay{
	 protected final Paint mPaint = new Paint();
	 protected final Paint mCirclePaint = new Paint();
	 private Bitmap gps_marker=null;
	 private Point mMapCoords = new Point();
	 private final float gps_marker_CENTER_X;
	 private final float gps_marker_CENTER_Y;
	private Canvas canvas;
	private MapView mapView;
	private boolean shadow;
	private GeoPoint geoPoint;
	
	 public MyLocationOverlayProxy(Context context, MapView mapView) {
		gps_marker = ((BitmapDrawable) context.getResources().getDrawable(
				R.drawable.marker_gpsvalid)).getBitmap();
		gps_marker_CENTER_X = gps_marker.getWidth() / 2 - 0.5f;
		gps_marker_CENTER_Y= gps_marker.getHeight() / 2 - 0.5f;
	 }	
	@Override
	public void draw(Canvas arg0, MapView arg1, boolean arg2) {
		super.draw(arg0, arg1, arg2);
		this.canvas=arg0;
		this.mapView=arg1;
		this.shadow=arg2;
		 Projection pj=mapView.getProjection();
	        if (this.geoPoint != null) {
	                mMapCoords=pj.toPixels(this.geoPoint, null);
					canvas.drawBitmap(gps_marker, mMapCoords.x-gps_marker_CENTER_X, mMapCoords.y-gps_marker_CENTER_Y, this.mPaint);
			}
	}

	public void updateItemPosition(GeoPoint mLocation){
		this.geoPoint=mLocation;
		this.draw(canvas, mapView, shadow);
	}
}
