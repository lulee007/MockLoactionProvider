package com.szmap.network;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.mapabc.mapapi.core.GeoPoint;

public class TransGDCoords {

	private final int TIME_OUT_DELAY = 3000;
	private Handler updateOverlayItemHandler;
	private CallBackUpdateItem updateItemCallback;

	/**根据gps坐标获取高德api后的偏移后的坐标
	 * @param updateItemCallback
	 */
	public TransGDCoords(final CallBackUpdateItem updateItemCallback) {
		this.updateItemCallback = updateItemCallback;
		updateOverlayItemHandler = new Handler() {

			@Override
			public void handleMessage(Message msg) {

				String verstring = (String) msg.obj;
				if (verstring != null) {
					try {
						// 获取VerJson文件
						HashMap<String, String> mHashMap = parseVerJson(verstring);

						if (mHashMap == null)
							return;
						if (mHashMap.get("message").equalsIgnoreCase("ok")) {
							int x = (int) (Double.valueOf((String) mHashMap
									.get("xx")) * 1E6);
							int y = (int) (Double.valueOf((String) mHashMap
									.get("yy")) * 1E6);
							GeoPoint pnt = new GeoPoint(y, x);
							updateItemCallback.callback(pnt);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}

		};
	}

	/**根据gps坐标获取高德api后的偏移后的坐标
	 * @param geo
	 */
	public void updateOverlayItem(final GeoPoint geo) {
		if (geo == null)
			return;
		new Thread(new Runnable() {

			@Override
			public void run() {
				String url = "http://112.25.187.198:9090/sisserver?config=RGC&flag=true&resType=json&cr=0&x1="
						+ (geo.getLongitudeE6() / 1E6)
						+ "&y1="
						+ (geo.getLatitudeE6() / 1E6)
						+ "&a_k=ab745991120327b360b1897fad16dc5a6a0f50b6bd307a3ec329e10a2cff8fb87480823da114f8f4";
				String jsonStr = getJsonbyHttp(url);
				if (jsonStr != null) {
					Message msg = new Message();
					msg.obj = jsonStr;
					updateOverlayItemHandler.sendMessage(msg);
				}
			}
		}).start();
	}

	public interface CallBackUpdateItem {
		abstract void callback(GeoPoint geo);
	}

	/*
	 * HTTP 访问 获取verJson
	 */
	private String getJsonbyHttp(String url) {
		String verstring = null;
		final HttpClient client = new DefaultHttpClient();
		final HttpGet get = new HttpGet(url);
		client.getParams().setIntParameter(HttpConnectionParams.SO_TIMEOUT,
				TIME_OUT_DELAY); // 超时设置
		client.getParams().setIntParameter(
				HttpConnectionParams.CONNECTION_TIMEOUT, TIME_OUT_DELAY);// 连接超时
		try {

			HttpResponse response = client.execute(get);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			for (String s = reader.readLine(); s != null; s = reader.readLine()) {
				Log.v("response", s);
				verstring = s;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return verstring;
	}


	/** 解析坐标Json
	 * @param strVerJson
	 * @return
	 * @throws JSONException
	 */
	private HashMap<String, String> parseVerJson(String strVerJson)
			throws JSONException {
		JSONObject jsonObj = new JSONObject(strVerJson);
		JSONArray jsonArray=jsonObj.getJSONArray("list");
		String appname = jsonObj.getString("message");
		String xx = ((JSONObject)jsonArray.get(0)).getString("x");
		String yy =((JSONObject)jsonArray.get(0)).getString("y");
		HashMap<String, String> ver = new HashMap<String, String>();
		ver.put("message", appname);
		ver.put("xx", xx);
		ver.put("yy", yy);
		return ver;
	}
}
