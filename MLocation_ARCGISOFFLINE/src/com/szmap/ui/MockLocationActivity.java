package com.szmap.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.MapOptions;
import com.esri.android.map.MapOptions.MapType;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISLocalTiledLayer;
import com.esri.android.map.event.OnSingleTapListener;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.Polyline;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.SimpleLineSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol.STYLE;
import com.szmap.controller.SZMapActivity;
import com.szmap.controller.SZMapMockLocationService;
import com.szmap.controller.Task;
import com.szmap.mlocation_arcgisoffline.R;
import com.szmap.model.CallBackBundle;
import com.szmap.model.CallBackDrawGraphic;
import com.szmap.model.DrawTool;
import com.szmap.model.OpenFileHelper;

public class MockLocationActivity extends Activity implements SZMapActivity {

	private static final String LOG_TAG = "mockLocation";
	private static final String localPath = "file:///mnt/sdcard/voicemapSH/1505003/1505003/Layers";
	private static final int DRAWMODEL = 0x997;
	private static final int FILEMODEL = 0x996;
	protected static final int ACTION_APPLICATION_DEVELOPMENT_SETTINGS_ID = 0x995;
	private Button buttonstart;
	List<String> data;
	private Button buttonstop;
	private Button buttonpause;
	private Button buttonwait;
	private Button buttoncontinue;
	private Button buttonopenfile;
	private String gpsFilePath;
	static private int OpenFileHelperId = 0;
	private final String MockLocationService = "com.szmap.controller.SZMapMockLocationService";
	private MapView mainMapView;
	private GraphicsLayer gpsLayer;
	private GraphicsLayer gpsTrackLayer;
	private int gpsGraphicID = -0x999;
	private int gpsTrackraphicID = -0x998;
	private DrawTool drawtool = null;
	private CallBackDrawGraphic callbackdrawgraphic = null;
	private Button buttonmove;
	private Button buttonsave;
	private Button buttondraw;
	private Button buttoncomplete;
	protected int selectedModelID = -0x996;
	private LinearLayout ll_maptoolbar;
	protected int gpsspeed_current = 0;
	protected int gpsspeed_final = 4;
	// private ArcGISTiledMapServiceLayer tiledMapService;
	private ArcGISLocalTiledLayer local;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initViews();
		initListeners();
		initParams();
	}

	/**
	 * 初始化数据和参数
	 */
	@Override
	public void initParams() {
		// 如果权限允许，就开启GPS模拟服务
		if (isMockLocationSet()) {
			// 开启服务
			startMockLocationService();
		}
		SZMapMockLocationService.activities.add(this);
		// 设置回调函数，绘图
		callbackdrawgraphic = new CallBackDrawGraphic() {

			@Override
			public void callback(Graphic gr) {
				Log.v(LOG_TAG, "callbackdrawgraphic");
				gpsTrackLayer.removeAll();
				SimpleLineSymbol lineSymbol = (SimpleLineSymbol) gr.getSymbol();
				lineSymbol.setColor(Color.DKGRAY);
				gpsTrackraphicID = gpsTrackLayer.addGraphic(new Graphic(gr
						.getGeometry(), lineSymbol));
				mainMapView.postInvalidate();
			}
		};

		// 初始化地图控件
		// tiledMapService = new ArcGISTiledMapServiceLayer(
		// "http://cache1.arcgisonline.cn/arcgis/rest/services/ChinaOnlineCommunity/MapServer");
		local = new ArcGISLocalTiledLayer(localPath);

		gpsLayer = new GraphicsLayer();
		gpsTrackLayer = new GraphicsLayer();
		mainMapView.addLayer(local, 0);
		mainMapView.addLayer(gpsTrackLayer, 1);
		mainMapView.addLayer(gpsLayer, 2);
		mainMapView.setMapOptions(new MapOptions(MapType.STREETS, 31, 118, 10));

		// 设置绘制监听事件
		drawtool = new DrawTool(this.mainMapView, callbackdrawgraphic);
		// 初始化为文件模式
		initSelectedModel(FILEMODEL);
	}

	/**
	 * 启动 GPS模拟服务
	 */
	private void startMockLocationService() {
		Intent serviceIntent = new Intent(MockLocationActivity.this,
				SZMapMockLocationService.class);
		if (checkMockLocationService())
			this.stopService(serviceIntent);
		this.startService(serviceIntent);
		Log.v(LOG_TAG, "try to start a service ");
	}

	private boolean isMockLocationSet() {
		if (Settings.Secure.getString(this.getContentResolver(),
				Settings.Secure.ALLOW_MOCK_LOCATION).contentEquals("1")) {
			Log.v(LOG_TAG, "MockLocation is enable");
			return true;
		} else {
			Log.v(LOG_TAG, "MockLocation is disable");
			return false;
		}
	}

	/**
	 * 检查MockLocationService是否已经启动
	 */
	private boolean checkMockLocationService() {
		ActivityManager activityManager = (ActivityManager) this
				.getSystemService(ACTIVITY_SERVICE);
		List<ActivityManager.RunningServiceInfo> serviceList = activityManager
				.getRunningServices(Integer.MAX_VALUE);
		if (!(serviceList.size() > 0)) {
			return false;
		}
		for (int i = 0; i < serviceList.size(); i++) {
			if (serviceList.get(i).service.getClassName().equals(
					MockLocationService) == true) {
				return true;
			}

		}
		return false;
	}

	/**
	 * 初始化控件的监听事件
	 */
	@Override
	public void initListeners() {
		mainMapView.setOnSingleTapListener(new OnSingleTapListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -4848151940478796358L;

			@Override
			public void onSingleTap(float arg0, float arg1) {

				// Point tapPoint= (Point)
				// GeometryEngine.project(mainMapView.toMapPoint(arg0, arg1),
				// SpatialReference.create(102100),
				// SpatialReference.create(4326));
				// Toast.makeText(MockLocationActivity.this,
				// " 坐标为："+tapPoint.getX()+","+tapPoint.getY(),
				// Toast.LENGTH_SHORT).show();
			}
		});
		buttonstart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!isMockLocationSet()) {
					dialogSetMockLocation();
					return;
				}
				// 初始化为文件模式
				initSelectedModel(selectedModelID);
				if (selectedModelID == DRAWMODEL) {
					SZMapMockLocationService.startMockLocation(data);
					changeToolbarState(1);
					ll_maptoolbar.setVisibility(View.GONE);
					buttonopenfile.setVisibility(View.VISIBLE);
				} else if (selectedModelID == FILEMODEL && initGPSData()) {
					SZMapMockLocationService.startMockLocation(data);
					changeToolbarState(1);
				} else
					Toast.makeText(MockLocationActivity.this, "打开文件错误请重新选择",
							Toast.LENGTH_SHORT).show();
			}

		});
		buttonpause.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				changeToolbarState(2);
				SZMapMockLocationService.pauseMockLocation();
			}
		});
		buttonwait.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				changeToolbarState(3);
				SZMapMockLocationService.waitMockLocation();
			}
		});
		buttonstop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// 初始化为文件模式
				initSelectedModel(FILEMODEL);
				ll_maptoolbar.setEnabled(true);
				changeToolbarState(5);
				SZMapMockLocationService.stopMockLocation();
			}
		});
		buttoncontinue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				changeToolbarState(4);
				SZMapMockLocationService.continueMockLocation();
			}
		});
		buttonopenfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				{
					changeToolbarState(0);
					showDialog(OpenFileHelperId);
				}
			}
		});
		buttonmove.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (drawtool.disablePan) {
					drawtool.disablePan = false;

				} else {
					drawtool.disablePan = true;
				}
				changeMapToolBarStates(0);
			}
		});
		buttonsave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				changeMapToolBarStates(3);
				if (checkGPSTrackData()) {
					showSaveDialog();
					changeToolbarState(0);
				}
			}
		});
		buttondraw.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (drawtool.draw_begin) {

					drawtool.deactivate();
				} else {

					drawtool.activate(DrawTool.FREEHAND_POLYLINE);
				}
				changeMapToolBarStates(1);
			}
		});
		buttoncomplete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				drawtool.sendDrawEndEvent(true);
				changeMapToolBarStates(2);
			}
		});
	}

	/**
	 * 将在地图上画线获得的坐标 保存起来
	 */
	protected void showSaveDialog() {
		selectedModelID = DRAWMODEL;
		SimpleDateFormat formatter = new SimpleDateFormat(
				"yyyy年MM月dd日HH-mm-ss", Locale.CHINESE);
		Date curDate = new Date(System.currentTimeMillis());// 获取当前时间
		String str = formatter.format(curDate);
		LayoutInflater factory = LayoutInflater.from(MockLocationActivity.this);
		// 获得自定义对话框
		View diaView = factory.inflate(R.layout.savedialog, null);
		final Dialog dialog = new Dialog(MockLocationActivity.this);
		dialog.setContentView(diaView);
		dialog.setTitle("请输入要保存的文件名");
		((EditText) dialog.findViewById(R.id.et_savename)).setText(str);
		final EditText et_savename = (EditText) dialog
				.findViewById(R.id.et_savename);
		Button but_ok = (Button) dialog.findViewById(R.id.but_ok);
		Button but_cancel = (Button) dialog.findViewById(R.id.but_cancel);
		but_ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (et_savename == null
						|| et_savename.getText().toString().isEmpty()) {
					Toast.makeText(MockLocationActivity.this, "名称不能为空，请输入文件名",
							Toast.LENGTH_SHORT).show();
					return;
				}
				switch (OpenFileHelper.saveFiles(data, et_savename.getText()
						.toString())) {
				case 0:
					Toast.makeText(MockLocationActivity.this,
							"文件保存到" + OpenFileHelper.sAppFolder + "目录下",
							Toast.LENGTH_SHORT).show();
					break;
				case 1:
					Toast.makeText(MockLocationActivity.this,
							et_savename.getText().toString() + "已经存在，请修改后再保存",
							Toast.LENGTH_SHORT).show();
					return;
				case 2:
					Toast.makeText(MockLocationActivity.this, "文件出错",
							Toast.LENGTH_SHORT).show();
					break;
				}
				dialog.dismiss();

			}
		});
		but_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});
		dialog.show();
	}

	/**
	 * 检查从线中获取的GPS是否有效
	 * 
	 * @return 获取的GPS坐标是否有效 true 有效，false 无效
	 */
	protected boolean checkGPSTrackData() {
		if (data == null)
			data = new ArrayList<String>();
		else {
			data.clear();
		}
		Polyline gpsTrackRoute = (Polyline) gpsTrackLayer.getGraphic(
				gpsTrackraphicID).getGeometry();
		if (gpsTrackRoute == null) {
			return false;
		}
		for (int i = 0; i < gpsTrackRoute.getPointCount(); i++) {
			try {
				// Point pnt = (Point) GeometryEngine.project(
				// gpsTrackRoute.getPoint(i),
				// SpatialReference.create(102100),
				// SpatialReference.create(4326));
				// data.add(pnt.getX() + "," + pnt.getY());
				data.add(gpsTrackRoute.getPoint(i).getX() + ","
						+ gpsTrackRoute.getPoint(i).getY());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return true;
	}

	/**
	 * @param i
	 *            0移动，1画线，2完成，3保存
	 */
	protected void changeMapToolBarStates(int i) {
		buttonsave.setEnabled(true);
		buttoncomplete.setEnabled(true);
		buttondraw.setEnabled(true);
		buttonmove.setEnabled(true);
		switch (i) {
		case 0:
			if (drawtool.disablePan) {
				buttonmove.setText("移动");
				buttonsave.setEnabled(false);
			} else {
				buttonmove.setText("停止");
				buttonsave.setEnabled(false);
				buttoncomplete.setEnabled(false);
				buttondraw.setEnabled(false);
			}
			break;
		case 1:
			if (drawtool.draw_begin) {
				buttondraw.setText("取消");
				buttonsave.setEnabled(false);
			} else {
				buttondraw.setText("编辑");
				buttonsave.setEnabled(false);
				buttoncomplete.setEnabled(false);
				buttonmove.setEnabled(false);
			}
			break;
		case 2:
			buttonmove.setEnabled(false);
			if (drawtool.draw_begin) {
				buttondraw.setText("取消");
			} else {
				buttondraw.setText("编辑");
			}
			break;
		case 3:
			buttonmove.setEnabled(false);
			buttoncomplete.setEnabled(false);
			break;
		default:
			break;
		}

	}

	private void initSelectedModel(int modelID) {
		SZMapMockLocationService.stopMockLocation();
		changeToolbarState(-1);
		changeMapToolBarStates(1);
		switch (modelID) {
		case DRAWMODEL:
			gpsFilePath = null;
			selectedModelID = DRAWMODEL;
			buttonopenfile.setVisibility(View.GONE);
			ll_maptoolbar.setVisibility(View.VISIBLE);
			break;
		case FILEMODEL:
			selectedModelID = FILEMODEL;
			buttonopenfile.setVisibility(View.VISIBLE);
			ll_maptoolbar.setVisibility(View.GONE);
			break;
		}
	}

	protected boolean initGPSData() {
		if (gpsFilePath == null)
			return false;
		File gpsfile = new File(gpsFilePath);
		if (gpsfile.exists() && gpsfile.isFile()) {
			BufferedReader reader = null;
			try {
				data = new ArrayList<String>();
				InputStream is = new FileInputStream(gpsfile);
				reader = new BufferedReader(new InputStreamReader(is));
				String line = null;
				while ((line = reader.readLine()) != null) {
					data.add(line);
				}
				Log.v(LOG_TAG, data.size() + " lines");
				reader.close();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
				if (reader != null) {
					try {
						reader.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		}
		return false;
	}

	/**
	 * 初始化控件
	 */
	@Override
	public void initViews() {
		buttonmove = (Button) findViewById(R.id.but_move);
		buttonsave = (Button) findViewById(R.id.but_save);
		buttondraw = (Button) findViewById(R.id.but_draw);
		buttoncomplete = (Button) findViewById(R.id.but_complete);
		mainMapView = (MapView) findViewById(R.id.mainMapView);
		buttonstart = (Button) findViewById(R.id.but_start);
		buttonstop = (Button) findViewById(R.id.but_stop);
		buttonpause = (Button) findViewById(R.id.but_pause);
		buttonwait = (Button) findViewById(R.id.but_wait);
		buttoncontinue = (Button) findViewById(R.id.but_continue);
		buttonopenfile = (Button) findViewById(R.id.but_openfile);
		ll_maptoolbar = (LinearLayout) findViewById(R.id.ll_maptoolbar);

	}

	/**
	 * 响应toolbar的控件状态
	 * 
	 * @param operID
	 *            0 打开文件；1开始；2暂停；3等待；4继续；5停止;默认 禁用所有
	 */
	private void changeToolbarState(int operID) {
		buttonstart.setEnabled(false);
		buttonpause.setEnabled(false);
		buttonwait.setEnabled(false);
		buttoncontinue.setEnabled(false);
		buttonstop.setEnabled(false);
		switch (operID) {
		case 0:
			buttonstart.setEnabled(true);
			break;
		case 1:
			buttonpause.setEnabled(true);
			buttonwait.setEnabled(true);
			buttonstop.setEnabled(true);
			break;
		case 2:
			buttoncontinue.setEnabled(true);
			buttonstop.setEnabled(true);
			break;
		case 3:
			buttoncontinue.setEnabled(true);
			buttonstop.setEnabled(true);
			break;
		case 4:
			buttonpause.setEnabled(true);
			buttonwait.setEnabled(true);
			buttonstop.setEnabled(true);
			break;
		case 5:
			buttonstart.setEnabled(true);
			break;
		default:
			break;
		}

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == OpenFileHelperId) {
			Map<String, Integer> images = new HashMap<String, Integer>();
			// 下面几句设置各文件类型的图标， 需要你先把图标添加到资源文件夹
			images.put(OpenFileHelper.sRoot, R.drawable.filedialog_root); // 根目录图标
			images.put(OpenFileHelper.sParent, R.drawable.filedialog_folder_up); // 返回上一层的图标
			images.put(OpenFileHelper.sFolder, R.drawable.filedialog_folder); // 文件夹图标
			images.put("txt", R.drawable.filedialog_file); // wav文件图标
			images.put(OpenFileHelper.sEmpty, R.drawable.filedialog_root);
			OpenFileHelper.checkFolder();
			Dialog dialog = OpenFileHelper.createDialog(id, this, "打开文件",
					new CallBackBundle() {
						@Override
						public void callback(Bundle bundle) {
							getFile(bundle);
						}
					}, ".txt;", images);
			dialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					if (gpsFilePath == null) {
						changeToolbarState(-1);
					} else {
						changeToolbarState(0);
					}

				}
			});
			return dialog;
		}
		return null;
	}

	/**
	 * 去设置GPS允许模拟位置
	 * 
	 */
	protected void dialogSetMockLocation() {
		AlertDialog.Builder builder = new Builder(MockLocationActivity.this);
		builder.setMessage("未将设置---开发--“允许模拟位置”勾选，现在就去设置吗？");
		builder.setTitle("提示");
		builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				Intent appDeploymentInten = new Intent(
						android.provider.Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS);
				if (appDeploymentInten != null)
					startActivityForResult(appDeploymentInten,
							ACTION_APPLICATION_DEVELOPMENT_SETTINGS_ID);
			}
		});

		builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}

		});

		builder.create().show();
	}

	/**
	 * 获得选择的文件完整名称
	 * 
	 * @param bundle
	 */
	protected void getFile(Bundle bundle) {
		gpsFilePath = bundle.getString("path");
		if (gpsFilePath != null)
			selectedModelID = FILEMODEL;
		Toast.makeText(this, gpsFilePath, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case ACTION_APPLICATION_DEVELOPMENT_SETTINGS_ID:
			Log.v(LOG_TAG, "ACTION_APPLICATION_DEVELOPMENT_SETTINGS returned");
			if (isMockLocationSet()) {
				startMockLocationService();
			} else {
				Log.v(LOG_TAG,
						"ACTION_APPLICATION_DEVELOPMENT_SETTINGS returned but not setted");
			}
			break;
		default:
			super.onActivityResult(requestCode, resultCode, data);
			break;
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		SZMapMockLocationService.removeActivity(this);
		Intent serviceIntent = new Intent(MockLocationActivity.this,
				SZMapMockLocationService.class);
		MockLocationActivity.this.stopService(serviceIntent);
		Log.v(LOG_TAG, "try to stop service");
		System.exit(0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_textmodel:
			initSelectedModel(DRAWMODEL);
			break;
		case R.id.action_mapmodel:
			initSelectedModel(FILEMODEL);
			break;
		case R.id.action_quit:
			this.finish();
			break;
		case R.id.action_speed:
			changeGPSSpeed();
			break;
		case R.id.action_about:
			aboutit();
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		// 返回true表示处理完菜单项的事件，不需要将该事件继续传播下去了
		return true;
	}

	private void aboutit() {
		AlertDialog.Builder builder = new Builder(MockLocationActivity.this);
		builder.setMessage(getResources().getString(R.string.fixbuglist));
		builder.setTitle(getResources().getString(R.string.action_about));
		builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.create().show();

	}

	/**
	 * 调节GPS发送的速度
	 */
	private void changeGPSSpeed() {
		LayoutInflater factory = LayoutInflater.from(MockLocationActivity.this);
		// 获得自定义对话框
		View diaView = factory.inflate(R.layout.gpsspeeddialog, null);
		final Dialog dialog = new Dialog(MockLocationActivity.this);
		dialog.setContentView(diaView);
		dialog.setTitle("调节GPS发送速度");
		Button but_ok = (Button) dialog.findViewById(R.id.but_ok);
		Button but_cancel = (Button) dialog.findViewById(R.id.but_cancel);
		final TextView tv_gpsspeed = (TextView) dialog
				.findViewById(R.id.tv_gpsspeed);
		SeekBar seekbarGPSSpeed = (SeekBar) dialog
				.findViewById(R.id.seekbar_gpsspeed);
		seekbarGPSSpeed.setMax(12);
		seekbarGPSSpeed.incrementProgressBy(2);
		seekbarGPSSpeed.setProgress(gpsspeed_final);
		DecimalFormat df = new DecimalFormat("##0.00");
		tv_gpsspeed.setText("当前速度为：" + df.format(1.0 * gpsspeed_final / 4)
				+ "倍");

		but_ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				gpsspeed_final = gpsspeed_current;
				SZMapMockLocationService
						.changeGPSSpeed(1.0 * gpsspeed_final / 4);
				dialog.dismiss();

			}
		});
		but_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});
		seekbarGPSSpeed
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						gpsspeed_current = progress == 0 ? 1 : progress;
						DecimalFormat df = new DecimalFormat("##0.00");
						tv_gpsspeed.setText("当前速度为："
								+ df.format(1.0 * gpsspeed_current / 4) + "倍");

					}
				});
		dialog.show();

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		PackageManager pm = getPackageManager();
		ResolveInfo homeInfo = pm.resolveActivity(
				new Intent(Intent.ACTION_MAIN)
						.addCategory(Intent.CATEGORY_HOME), 0);
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			ActivityInfo ai = homeInfo.activityInfo;
			Intent startIntent = new Intent(Intent.ACTION_MAIN);
			startIntent.addCategory(Intent.CATEGORY_LAUNCHER);
			startIntent
					.setComponent(new ComponentName(ai.packageName, ai.name));
			startActivitySafely(startIntent);
			return true;
		} else
			return super.onKeyDown(keyCode, event);
	}

	private void startActivitySafely(Intent intent) {
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		try {
			startActivity(intent);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
		} catch (SecurityException e) {
			Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void refresh(Object... param) {
		switch (((Integer) param[0]).intValue()) {
		case Task.TASK_REFRESHGPS:
			Location location = (Location) param[1];
			// Point pt = (Point) GeometryEngine.project(new Point(
			// location.getLongitude(), location.getLatitude()),
			// SpatialReference.create(4326), SpatialReference
			// .create(102100));
			Point pt = new Point(location.getLongitude(),
					location.getLatitude());
			if (location != null) {
				if (gpsLayer.getGraphic(gpsGraphicID) == null) {
					// removeGraphic(gpsGraphicID);
					SimpleMarkerSymbol gpsSymbol = new SimpleMarkerSymbol(
							Color.BLUE, 20, STYLE.CIRCLE);
					Graphic gpsGraphic = new Graphic(pt, gpsSymbol);
					gpsGraphicID = gpsLayer.addGraphic(gpsGraphic);
				} else {
					gpsLayer.updateGraphic(gpsGraphicID, pt);
				}
				mainMapView.centerAt(pt, true);
				mainMapView.postInvalidate();
			}
			break;
		default:
			break;
		}

	}

}
