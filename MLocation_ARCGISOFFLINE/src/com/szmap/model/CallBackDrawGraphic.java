package com.szmap.model;

import com.esri.core.map.Graphic;


public interface  CallBackDrawGraphic {
	abstract void callback(Graphic gr);  
}
